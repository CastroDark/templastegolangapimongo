package models

import (
	"time"
)

// Profile - is the memory representation of one user profile
type Profile struct {
	ID          string    `json:"id,omitempty" bson:"_id,omitempty"`
	Name        string    `json:"username"`
	Password    string    `json:"password"`
	Age         int       `json:"age"`
	LastUpdated time.Time `json:"lastTime"`
}

type LogUser struct {
	ID        string    `json:"id,omitempty" bson:"_id,omitempty"`
	IdUser    string    `json:"idUser,omitempty" bson:"iduser,omitempty"`
	IdCompany string    `json:"idCompany" bson:"idcompany"`
	Log       string    `json:"log" bson:"log"`
	Status    int       `json:"status" bson:"status"`
	Level     int       `json:"level" bson:"level"`
	Date      time.Time `json:"date" bson:"date"`
}

type LogSystem struct {
	ID     string    `json:"id,omitempty" bson:"_id,omitempty"`
	Log    string    `json:"log" bson:"log"`
	Status int       `json:"status" bson:"status"`
	Level  int       `json:"level" bson:"level"`
	Date   time.Time `json:"date" bson:"date"`
}
