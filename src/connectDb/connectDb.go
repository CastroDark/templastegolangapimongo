package connectDb

import (
	"context"
	"log"
	"os"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var ClientMongo *mongo.Client

func Connect() bool {

	//mongodb://castro:231154@172.16.18.121:27017

	urlDb := os.Getenv("URL_MONGODB")

	// Rest of the code will go here
	// Set client options
	clientOptions := options.Client().ApplyURI(urlDb)
	//Context = context.TODO()
	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)
	ClientMongo = client
	if err != nil {
		log.Fatal(err)
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
		return false
	}
	//fmt.Println("Connected to MongoDB!")
	return true

}

func Status() *mongo.Client {

	// Check the connection
	err := ClientMongo.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
		result := Connect()
		if result {
			return ClientMongo
		}
		return ClientMongo
	}

	return ClientMongo

}
