package conectmongo

import (
	"context"
	"log"
	"rest-api-golang/src/loggerservice"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var Client *mongo.Client

func Connect() bool {
	// Rest of the code will go here
	// Set client options
	clientOptions := options.Client().ApplyURI("mongodb://castro:231154@172.16.18.121:27017")
	//Context = context.TODO()
	// Connect to MongoDB
	client, err := mongo.Connect(context.TODO(), clientOptions)

	if err != nil {
		log.Fatal(err)
		loggerservice.Add(err.Error())
	}

	// Check the connection
	err = client.Ping(context.TODO(), nil)

	if err != nil {
		log.Fatal(err)
		loggerservice.Add(err.Error())
		return false
	}
	Client = client

	//fmt.Println("Connected to MongoDB!")

	return true

}

func Status() *mongo.Client {

	// Check the connection
	err := Client.Ping(context.TODO(), nil)
	if err != nil {
		log.Fatal(err)
		loggerservice.Add(err.Error())
		result := Connect()
		if result {
			return Client
		}
		return Client
	}

	return Client

}
